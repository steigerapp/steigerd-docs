.. _kumpel installation linux:

Linux
*********


.. _linux-manually:

##################################
Install script
##################################


.. code-block:: bash

  import os
  customer_key = ""
  access_key = ""

  os.system("sudo apt-get install ipmitool")
  os.system("sudo mkdir /opt/kumpel")
  os.system("sudo chmod 777 /opt/kumpel")
  os.chdir('/opt/kumpel')
  os.system("sudo wget https://download.steiger.app/kumpel-linux.gz")
  os.system("gunzip kumpel-linux.gz")
  os.system("sudo chmod 777 kumpel-linux")
  os.system("touch /opt/kumpel/kumpel.json")

  kumple_json ='''
  {
    "customerKey": "''' + customer_key + '''",
    "accessKey": "''' + access_key + '''",
    "dhcp": {
    "enabled": false
  }
  }
  '''
  with open("/opt/kumpel/kumpel.json", "w+") as file:
    file.write(kumple_json)
  kumpel_conf = '''
  [Unit]
  Description=Kumpel
  After=network.target multi-user.target
  [Service]
  ExecStart=/opt/kumpel/kumpel-linux
  Restart=always
  User=root
  Group=root
  WorkingDirectory=/opt/kumpel/
  [Install]
  WantedBy=multi-user.target
  '''
  with open("/etc/systemd/system/kumpel.service", "w+") as file:
    file.write(kumpel_conf)

  os.system("sudo systemctl daemon-reload")
  os.system("sudo systemctl enable kumpel")
  os.system("sudo systemctl start kumpel")

================================
Kumpel directory
================================

A typical Kumpel directory contains the following files:

.. code-block:: bash

  4.0K -rw-r--r-- 1 kumpel kumpel  484 Oct 22 13:35 kumpel.json
  4.8M -rw-r--r-- 1 kumpel kumpel 4.8M Oct 26 16:24 error.log
  195M -rwxrwx--- 1 kumpel kumpel 195M Sep 29 18:37 kumpel
  4.0K drwxr-xr-x 2 kumpel kumpel 4.0K Oct 25 17:01 storage
  4.0K drwxr-xr-x 2 kumpel kumpel 4.0K Oct 25 17:01 cache
  4.8M -rw-r--r-- 1 kumpel kumpel 4.8M Oct 26 16:24 warn.log
