.. _intro eisen:

Eisen 
######

Eisen is a small hardware device that helps you label your units properly before you even put them onto shelves. 
This makes big scale deployments of miners more streamlined.

The basic workflow is:

1. Setup Eisen (see documentation below)
#. Connect your smartphone with the Steiger mobile app and label printer to Eisen
#. Plug a miner into Eisen's Ethernet port
#. Follow the on screen instructions of the mobile app.
#. As soon as the label is being printed, the miner is in the database

The complete process takes less then 30 seconds per miner:


.. include:: video-eisen.rst

noticed the tiny black box? That's Eisen.


.. toctree::
   :maxdepth: 1
   :caption: more detailed information:

   eisen-installation
   eisen-userguide
