.. _user dashboard:

Dashboard
**************

The dashboard is the place where all information is being gathered for your convenience. It's usually the first thing you see once you log in.

With a fresh account it might look somewhat like this:

.. figure:: ../_images/user_emptydashboard.png
    :width: 500px
    :align: center
    :alt: empty dashboard

    *empty dashboard*


Once you have a few units and the power goes down in one of your locations, it might look somewhat like this:

.. figure:: ../_images/user_issuedashboard.png
    :width: 500px
    :align: center
    :alt: issue dashboard

    *dashboard with issue*