.. _new farms:

Setting up a new farm
**********************

You're starting from scratch. Miners have not been configured yet or you're overhauling your setup to improve manageability.

Checklist
----------

1. You have an :ref:`account<user account>` on the Steiger platform
#. :ref:`Kumpel<intro kumpel>` is installed, :ref:`configured<kumpel configuration>` and running on the same network as your miners, :ref:`DHCP is enabled<dhcp_enabled>`
#. There are 2 options now: 

  a. Use our setup tool :ref:`Eisen<intro eisen>` to make use of the labelling and inventorising feature or 
  #. Just plug your machines into the network and omit these features

  Both variants are viable and it depends on your need, which is best for your. The more machines you have, the more useful it becomes to have them properly labelled & inventorised.

4. And that's it! The miners connected to the network will get an IP address from Kumpel's built in DHCP server, Steiger will roll out the configuration you set through the initial wizard to all machines and they start mining immediately! You skipped the initial setup? No problem, you can still :ref:`add the configuration<user algorithms>` now.


Labelling & inventorising
-------------------------

As mentioned above, we integrated a setup process for new farms, that will drastically reduce the time you get your new machines up and running.
Using the setup process will also enable you to label every miner which makes localising faulty machines a lot easier later on. 
You can of course label your miners later using the same feature, but we find it to be a lot easier if you do it before the unit sits on a shelve.
Try it out, it takes only 30 seconds, even including fiddling with the label ;)

.. include:: video-eisen.rst
