.. _user unittypes:

Unit Types
**************

Manage & configure your unit types (Antminer S9 etc.) here. 

.. figure:: ../_images/user_unittypes.png
    :width: 500px
    :align: center
    :alt: unittypes view

    *unit types*

.. TODO: unit types description