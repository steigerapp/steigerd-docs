.. _existing farms:

Integrating Steiger in existing farms
*************************************

You have an existing farm you want to manage with Steiger. Your network is configured and you use DHCP to assign IP addresses to your miners.

Checklist
----------

1. You have an :ref:`account<user account>` on the Steiger platform
#. :ref:`Kumpel<intro kumpel>` is installed, :ref:`configured<kumpel configuration>` and running on the same network as your miners, :ref:`DHCP is disabled<dhcp_disabled>`
#. Log into Steiger and :ref:`scan your network for miners<scanning for units>`
#. And that's it! Once the scanning is completed, Steiger will roll out the configuration you set through the initial wizard to all machines and they start mining immediately! You skipped the initial setup? No problem, you can still :ref:`add the configuration<user algorithms>` now.



