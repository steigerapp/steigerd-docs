.. _user users:

Users
**************

You can add multiple users to your account. This is the place to do that.


.. figure:: ../_images/user_users.png
    :width: 500px
    :align: center
    :alt: users view

    *users*

.. TODO: add detail view