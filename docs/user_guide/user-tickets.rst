.. _user tickets:

Tickets
**************

This is the place for system as well as user tickets. 

.. figure:: ../_images/user_tickets.png
    :width: 500px
    :align: center
    :alt: tickets view

    *tickets view*

##############
User Tickets
##############

manually created tickets for the colocation provider (see agency system)


##############
System Tickets
##############

tickets created automatically by the system in case units show anomalies