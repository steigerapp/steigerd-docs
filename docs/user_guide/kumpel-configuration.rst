.. _kumpel configuration:

****************************
Configuring Kumpel
****************************

.. contents:: Topics


This topic describes how to control Kumpel settings.


.. _the_configuration_file:

######################
Configuration file
######################

All settings in Kumpel are adjustable via a configuration file (kumpel.json). This file needs to be placed inside the Kumpel installation directory.

.. note::

    The agent will not start properly without the config file!

.. _dhcp_enabled:

Config file example, DHCP enabled:

.. code-block:: json

    {
      "customerKey": "your_customer_key",
      "accessKey": "your_access_key",
      "dhcp": {
        "enabled": true,
        "range": [
          "192.168.199.10", "192.168.199.200"
        ],
        "netmask": "255.255.255.0",
        "router": [
          "192.168.199.1"
        ],
        "dns": ["8.8.8.8", "1.1.1.1"],
        "broadcast": "192.168.199.255",
        "server": "192.168.199.1",
        "tftpServer": "192.168.199.1"
      }
    }

.. _dhcp_disabled:

Config file example, DHCP disabled:

.. code-block:: json

    {
      "customerKey": "your_customer_key",
      "accessKey": "your_access_key",
      "dhcp": {
        "enabled": false
      }
    }

+-------------------+------------------------------------------+----------+---------------+
| **variable name** | **description**                          | **type** | **mandatory** |
+-------------------+------------------------------------------+----------+---------------+
| accessKey         | your access key                          | string   | yes           |
+-------------------+------------------------------------------+----------+---------------+
| customerKey       | your customer key                        | string   | yes           |
+-------------------+------------------------------------------+----------+---------------+
| dhcp: enabled     | enable dhcp server                       | boolean  | yes           |
+-------------------+------------------------------------------+----------+---------------+
| dhcp: range       | upper & lower dhcp boundary ip addresses | array    | yes [1]_      |
+-------------------+------------------------------------------+----------+---------------+
| dhcp: netmask     | subnet mask for dhcp clients             | string   | yes [1]_      |
+-------------------+------------------------------------------+----------+---------------+
| dhcp: router      | default gateway for dhcp clients         | array    | yes [1]_      |
+-------------------+------------------------------------------+----------+---------------+
| dhcp: dns         | dns server(s)                            | array    | yes [1]_      |
+-------------------+------------------------------------------+----------+---------------+
| dhcp: broadcast   | broadcast address                        | string   | yes [1]_      |
+-------------------+------------------------------------------+----------+---------------+
| dhcp: server      | ip address of the agent itself           | string   | yes [1]_      |
+-------------------+------------------------------------------+----------+---------------+
| dhcp: tftpServer  | address of the TFTP server               | string   | yes [1]_      |
+-------------------+------------------------------------------+----------+---------------+


.. [1] if dhcp: enabled = true


Notes on the DHCP server
""""""""""""""""""""""""""

Kumpel has a built in DHCP server. If you use a dedicated network for your miners, it is encouraged that you use this feature because it automatically detects new devices on the network and adds them to the database.
If you already have a DHCP server on your network, you can deactivate it with the dhcp flag in the config file. If you add new miners to the network you need to scan the network manually to add these machines to the database.

.. TODO: create and add link to scanning page

.. warning::

    You should properly segment your network. This means either VLANs or subnets that are divided by a layer 3 switch / router **for each instance** of the agent.
    Each agent can handle at least 500 ASICs. However, broadcast traffic will start to congest your network earlier. We recommend broadcast domains not bigger than /23 subnets. 
    Keep that in mind when designing your network.
    If you need help implementing Kumpel at big farms, get in touch.

.. note::

    GPU miner support is not fully implemented in this early version and thus deactivated despite the values in the config file.