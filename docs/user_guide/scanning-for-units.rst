.. _scanning for units:

Scanning for units
**********************
 
In your :ref:`Kumpel detail view<user kumpeldetail>`, you'll find the green "Scan for units" button in the upper right corner:

.. figure:: ../_images/user_kumpeldetail_empty.png
    :width: 500px
    :align: center
    :alt: Kumpel detail view

    *Kumpel detail view*

Once you click on it, a form opens:

.. figure:: ../_images/scan_for_units_form1.png
    :width: 500px
    :align: center
    :alt: scan for units form

    *scan for units form*

In the CIDR network address field you enter your network address in CIDR notation e.g.: 192.168.0.1/24 and click "Scan Now".
Kumpel will scan the provided network range for hosts and adds them to the database. Kumpel will also try to detect what type of host it found. The most common ASICs are currently supported as well as our own GPU API, which can be installed on any GPU rig, with lots more to come.

.. TODO: more details on the GPU API