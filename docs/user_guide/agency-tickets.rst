.. _agency tickets:

Tickets
**************

In agency mode, users (customers of the agency) can create tickets to be assigned to the agency. 
This is useful for cases when the agency (like a colocation provider) manages the hardware for the customer.

.. TODO: agency description. 