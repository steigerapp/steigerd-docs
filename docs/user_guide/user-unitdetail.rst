.. _user unitdetail:

Unit Detail
**************

This is the detail view for each unit. It contains 

* temperature for each hashing board
* hash rate for each hashing board
* IP address
* MAC address
* location
* algorithm
* tickets appended to this machine
* activities (comparable to an audit log)


.. figure:: ../_images/user_unitdetail.png
    :width: 500px
    :align: center
    :alt: unit detail view

    *unit details*
