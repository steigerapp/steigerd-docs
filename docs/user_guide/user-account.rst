.. _user account:

Creating An Account
*********************

Creating an account is as easy as it gets:

#. go to `my.steiger.app <https://my.steiger.app>`_ 
#. click "REGISTER"
#. provide some basic information (you can add more users for the same account later)
#. confirm your email
#. On your first login a wizard will guide through you the setup process

