.. _manuals:

Manuals
**************

General
-----------

* :download:`BIOS Flash Manual <../_assets/manuals/BIOS Flash Manual.pdf>`
* :download:`BMC Flash Manual <../_assets/manuals/BMC Flash Manual.pdf>`
* :download:`BMC Login On-Site Manual <../_assets/manuals/BMC Login On-Site Manual.pdf>`
* :download:`GPU Filter for defect Manual <../_assets/manuals/GPU Filter for defect Manual.pdf>`
* :download:`GPU Find Defective Manual <../_assets/manuals/GPU Find Defective Manual.pdf>`
* :download:`GPU Swap Manual <../_assets/manuals/GPU Swap Manual.pdf>`
* :download:`HPC Installation Manual draft <../_assets/manuals/HPC Installation Manual draft.pdf>`
* :download:`Inventory Manual <../_assets/manuals/Inventory Manual.pdf>`
* :download:`Setup Colocation in Steiger Manual <../_assets/manuals/Setup Colocation in Steiger Manual.pdf>`
* :download:`Steiger Installation Manual <../_assets/manuals/Steiger Installation Manual.pdf>`
* :download:`Steiger Kumpel Scan Manual <../_assets/manuals/Steiger Kumpel Scan Manual.pdf>`
* :download:`Steiger Ready machines clean up Manual <../_assets/manuals/Steiger Ready machines clean up Manual.pdf>`

Data Center Deployment
-----------

* :download:`Site-Readiness-Checklist <../_assets/manuals/Mining site readiness checklist.pdf>`
* :download:`Getting access to Steiger <../_assets/manuals/Setup Steiger Account.pdf>`
* :download:`Setting up a mobile phone for Steiger <../_assets/manuals/Setup mobile phone.pdf>`
* :download:`Explanatory image for locations in Steiger <../_assets/manuals/Locations.jpg>`
* :download:`Inventorying with the Steiger Mobile App <../_assets/manuals/Inventorying with Steiger.pdf>`
* :download:`Setting locations for units with the Steiger Mobile App <../_assets/manuals/Adding locations to units in Steiger.pdf>`

Video-Guides
-----------

#. :download:`Introduction to Steiger <../_assets/manuals/Northern Data - Steiger Intro.mp4>`
#. :download:`Creating customers in Steiger <../_assets/manuals/Northern Data - Creating customers in Steiger.mp4>`
#. :download:`Preparing locations in Steiger <../_assets/manuals/Northern Data - Prepare Locations in Steiger.mp4>`
#. :download:`Labelling locations <../_assets/manuals/Northern Data - Labelling Locations.mp4>`
#. :download:`Scanning for units in Steiger <../_assets/manuals/Northern Data - Scanning for units.mp4>`
#. :download:`Setting unit locations in Steiger <../_assets/manuals/Northern Data - Setting unit locations.mp4>`