.. _user locations:

User Locations
**************

Manage your locations and their shelving setup.

.. figure:: ../_images/user_locations.png
    :width: 500px
    :align: center
    :alt: user locations

    *user locations*

.. include:: location-details.rst

.. include:: location-config.rst

