.. _user groups:

Groups
**************

For easy and quick access as well as reporting and monitoring reasons, you can combine units into groups. Configure and view these groups here.

.. figure:: ../_images/user_groups.png
    :width: 500px
    :align: center
    :alt: groups view

    *groups view*