.. _user units:

Units
**************

Units is the place where you find a list all of your miners (we call them "units"). You can :ref:`user units filter` the list by different cirteria.
You can also mark multiple units and apply a "bulk action", like restarting or deleting, to them.

If you want to know more about a certain unit, you can click on the ">" button to go into the :ref:`user unitdetail` view 

.. figure:: ../_images/user_units.png
    :width: 500px
    :align: center
    :alt: units view

    *units view*

.. toctree::
   :glob:

   Unit Details <user-unitdetail>
   Unit Filter <user-units-filter>

.. include:: unit-status.rst
