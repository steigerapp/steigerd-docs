.. _intro mobileapp:

Mobile App 
###########

In conjunction with the Steiger web app, we offer a mobile app (Android with iOS planned). 

The Steiger mobile app has 3 main purposes:

.. toctree::
   :glob:

   Location tagging <app-locationtagging>
   Notifications <app-notifications>
   Recording units <app-unitrecording>

