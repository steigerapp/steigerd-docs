.. _intro agency:

Agency
######

The Agency view is for users that manage units of other people, like colocation providers. 
You can manage multiple customers, process their tickets and provide them with their own dashboard from which they comprehensibly manage their machines themselves.

You can also log into their account to provide support.

If you want to use Steiger's agency system, get in touch! We're looking forward to hearing from you. 

.. toctree::
   :maxdepth: 1

   agency-hardware
   agency-tickets
   agency-locations
   agency-customers
   agency-users

