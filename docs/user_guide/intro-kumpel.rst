.. _intro kumpel:

Kumpel
######

  **Kum·pel**

  | /ˈkʊmpl̩,Kúmpel/
  | *noun, male* 

  A person who works in a mine

Kumpel is the local service for the Steiger Miner Management Suite  
It collects statistics from ASICs in the LAN and configures said miners in conjunction with Steiger. It can also act as a DHCP server (this feature can be deactivated if you use your own DHCP server).

.. toctree::
   :glob:

   Installation <kumpel-installation>
   Configuration <kumpel-configuration>
   Update <kumpel-update>
   