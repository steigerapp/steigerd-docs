.. _faq:

FAQ
**************

This is a list of Frequently Asked Questions about Steiger.  Feel free to
suggest new entries!


What do I need Kumpel for?
---------------------------

Steiger is the web app to manage all your things mining. However it needs someone to talk to at your facility since you obviously don't want to expose all your machines on the internet. That's where Kumpel comes into play.
Kumpel securely connects to Steiger using military grade encryption to send statistics (hash rate, temperature etc) and receive jobs (configure pool, restart etc).


