.. _intro gui:

Web GUI
############

In this section we briefly go over the web interface of Steiger and see what each element does.

.. toctree::
   :glob:

   Dashboard <user-dashboard>
   Units view <user-units>
   Groups view <user-groups>
   Unit Types view <user-unittypes> 
   Kumpels view <user-kumpels> 
   Locations view <user-locations>
   Algorithms view <user-algorithms>
   Tickets view <user-tickets>
   Users view <user-users>
   Account settings <user-account>


