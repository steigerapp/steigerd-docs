.. _getting started:


Getting Started
################

There are 2 main scenarios we see, when you start with Steiger:

1. You want to integrate Steiger into your existing farm or already have a DHCP server running in your miner network
2. You set up a new farm and do not have a DHCP server running in your miner network

To make your life easier, Steiger has a process for both scenarios:

.. toctree::
    :maxdepth: 1

    Existing Farms <existing-farms>
    New Farms <new-farms>
    

