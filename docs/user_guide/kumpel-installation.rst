.. _installation_guide:

Installing Kumpel
****************************

Welcome to the Kumpel Installation Guide!

.. contents:: Topics


.. _what_will_be_installed:

##################################
Basics / What Will Be Installed
##################################

Kumpel is a single binary, that is available for Linux, Windows, OSX on x86 systems and for Linux on ARMv7. 
Once downloaded, the only thing you need to to, is to create the config file. This is explained in the :ref:`kumpel configuration` section

.. _control_machine_requirements:

##################################
Control Machine Requirements
##################################

The following miner types are currently supported (this list will be expanded constantly):

- Ebit E9, Ebit E9+
- Antminer S9/i/j
- Antminer L3+
- Innosilicon A6, A5+
- Baikal Giant B

##################################
Installing the Kumpel agent
##################################


.. toctree::
   :glob:

   Linux <kumpel-installation-linux>
   Windows <kumpel-installation-windows>


.. On OSX
    **********
    `Download binary for OSX x86 <https://download.hashtrend.ch/kumpel-osx.gz>`_