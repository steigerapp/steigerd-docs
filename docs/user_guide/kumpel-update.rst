.. _update:

Updating Kumpel
****************************

Kumpel has an integrated update mechanism. 

To trigger the update, start Kumpel with the update command, like so on Linux:

.. code-block:: bash

    $ ./kumpel update


or on Windows:

.. code-block:: doscon

    C:\>kumpel.exe update

Kumpel always goes into to normal mode, when no update has been found or an update was successfully applied. Thus it is safe to always start Kumpel with the update flag.


