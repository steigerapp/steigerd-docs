.. _user kumpeldetail:

Kumpel detail view
*******************

Here you'll find the local management agents 'Kumpel' and their respective units.

.. figure:: ../_images/user_kumpeldetail_empty.png
    :width: 500px
    :align: center
    :alt: Kumpel detail view

    *Kumpel detail view*



