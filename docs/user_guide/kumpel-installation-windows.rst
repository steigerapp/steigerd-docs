.. _kumpel installation windows:

Windows
***************

.. _windows-manually:

==========
manually
==========

Download the binary with your browser, create config file, start. For ease of use it is recommended to create a service.
This is described in :ref:`this section <autostart_win>`

`Download binary for Win x86 <https://download.hashtrend.ch/kumpel-win.exe.zip>`_

================================
creating the config file
================================

You need to create a config file within the Kumpel installation directory. This is explained in the :ref:`kumpel configuration` section.

================================
Windows Defender warning
================================

On first run (depending on your Windows version) there will most likely be a warning, similar to this:

.. figure:: ../_images/windows_defender_1.png
    :width: 500px
    :align: center
    :alt: Windows Defender

    *Windows Defender warning*


To proceed, click on "More info". A link to "Run anyway" will appear. Click that to proceed:

.. figure:: ../_images/windows_defender_2.png
    :width: 500px
    :align: center
    :alt: Windows Defender

    *Windows Defender run anyway*


.. _autostart_win:

================================
Starting Kumpel on system start
================================

Autostart
-------------

If you just want to automatically start the steiger daemon once you log in, this is the easiest way.

* Press Win-R
* enter

.. code-block:: winbatch
    
    shell:startup

* a folder opens; drop a link to the Kumpel binary in there


With Resource Kit
-----------------

To create an actual service for Windows, refer to `this documentation from Microsoft on how to create a serivce on Windows. <https://support.microsoft.com/en-us/help/137890/how-to-create-a-user-defined-service>`_
