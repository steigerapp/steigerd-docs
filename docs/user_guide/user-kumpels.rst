.. _user kumpels:

Kumpels
**************

Here you'll find the local management agents 'Kumpel' and their respective units.

.. figure:: ../_images/user_kumpels.png
    :width: 500px
    :align: center
    :alt: Kumpels view

    *Kumpels view*

There's also a detail view which allows you to scan the network for new miners (you don't need to do this if you use Kumpel's built in DHCP server)

.. toctree::
   :glob:

   Kumpel detail view <user-kumpeldetail>
   Scanning for units <scanning-for-units>


