.. _glossary:

Glossary
**************

.. glossary::

  DHCP
    Dynamic Host Configuration Protocol; dynamically assigns IP addresses to hosts on a network

  Eisen
    setup helper that records your miner into the database and prints a label (using a connected label printer) to properly inventorise your machines 

  Kumpel
    local management agent featuring DHCP server which can be deactivated

  

  