.. _agency locations:

Agency Locations
*****************

Manage your customers locations and their respective shelving setup

.. figure:: ../_images/agency_locations.png
    :width: 500px
    :align: center
    :alt: agency locations

    *agency locations*

.. include:: location-details.rst

.. include:: location-config.rst


