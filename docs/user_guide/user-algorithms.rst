.. _user algorithms:

Algorithms
**************

This is the place to configure the various algorithms (SHA256, X11, Scrypt ...) and their respective pool settings.

You configure the pool for any algorithm just once and it will automatically be set on all units that use this particular algorithm.


.. figure:: ../_images/user_algorithms.png
    :width: 500px
    :align: center
    :alt: algorithms

    *algorithms*

.. TODO: algorithm detail view & explanation