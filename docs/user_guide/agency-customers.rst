.. _agency customers:

Customers
**************

This view lets gives you an overview over all the customers and their hardware. You can also log into their account (2FA secured) to provide first level support.

.. figure:: ../_images/agency_customers.png
    :width: 500px
    :align: center
    :alt: customers view

    *customers view*

.. TODO: customers detail view 

