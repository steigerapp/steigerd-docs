Status
-------

Online
^^^^^^^^^^^^
The unit is mining properly without problems

.. figure:: ../_images/unit_online.png
    :width: 100px


Issues
^^^^^^^^^^^^
The unit has one or more problems (not all hashing boards online, temperature above threshold, hash rate below threshold)

.. figure:: ../_images/unit_issues.png
    :width: 100px

Offline
^^^^^^^^^^^^
The unit has not reacted to :ref:`Kumpels<intro kumpel>` requests and thus is considerd offline by the system

.. figure:: ../_images/unit_offline.png
    :width: 100px
    
Ready
^^^^^^^^^^^^
The unit has been initially recorded into the database but has yet to go online for the first time. 
These units are not being listed in the units view per default. You can choose to do so by unchecking the box 'Exclude units marked as "ready"' above the units list.

.. figure:: ../_images/unit_ready.png
    :width: 100px

Ghost
^^^^^^^^^^^^
The :ref:`Kumpel<intro kumpel>` the unit belongs to hasn't sent data to the backend for a while and thus is considered offline

.. figure:: ../_images/unit_ghost.png
    :width: 100px

