
.. figure:: ../_images/arrow.png
    :width: 100px
    
    go to the location detail view    

.. figure:: ../_images/qrcode.png
    :width: 100px

    print QR codes for location tagging

.. figure:: ../_images/bell.png
    :width: 100px

    mute notifications for this location


