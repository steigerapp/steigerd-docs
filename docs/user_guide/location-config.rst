
.. figure:: ../_images/location_config.png
    :width: 500px

To be able to properly assign your units to the particular setup you have, you can configure the way your shelves or racks are structured in this view. 

Title
^^^^^^^^^^^^
To give your location a name, click on the pen next to it.

Country
^^^^^^^^^^^^
The country your location is in

Facility
^^^^^^^^^^^^
A name for the whole facility all the shelves are in, like "3 Abby Road"

Amount Of Walls
^^^^^^^^^^^^^^^
.. figure:: ../_images/location_customizer_side.png
    :width: 300px

How many walls does one entity in your location have? For example in a classic shelve, put 1. If you use a datacenter pod with 2 rows of racks, put 2 etc. 
This even allows you to get crazy with exotic custom setups like octagonal towers. 


Amount Of Columns
^^^^^^^^^^^^^^^^^^^^
.. figure:: ../_images/location_customizer_column.png
    :width: 300px

Into how many columns is your shelving divided? Datecenter pods often have 10 racks side by side - in that case you'd put a 10 here.

Amount Of Rows
^^^^^^^^^^^^^^^^^^^^
.. figure:: ../_images/location_customizer_row.png
    :width: 300px

Rows is the number stories of your shelving. 

Units per QR ("section")
^^^^^^^^^^^^^^^^^^^^^^^^
.. figure:: ../_images/location_customizer_section.png
    :width: 300px

The intersection of column and row constitutes a section. You can assign a QR code to a section for easy location.
In this field you can configure, how many units will fit into one of these sections. 

.. note:: 

    it makes sense to keep this number in a reasonable range. Later on, the app can tell you where a unit is located by it's section. 
    Having a section just for one unit each creates lots of locations and will make your dashboard cluttered and hard to read. 
    Having 200 units in one section will make it hard to find the defective unit. We found that low 2 digit numbers worked best.
    Keep in mind that you have *rows* x this number of units in one column!


