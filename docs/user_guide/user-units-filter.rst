.. _user units filter:

Filter
**************

In the advanced filter box you can chain multiple filters to select the units you want (like all the units with issues in a certain location)

.. figure:: ../_images/user_unitfilters.png
    :width: 500px
    :align: center
    :alt: advanced filter box

    *advanced filter box*

.. include:: unit-status.rst

