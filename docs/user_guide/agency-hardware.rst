.. _agency hardware:

Hardware
**************

This page gives you an overview over the status of the hardware of all your customers, aggregated by hardware type.

.. figure:: ../_images/agency_hardware.png
    :width: 500px
    :align: center
    :alt: agency hardware view

    *agency hardware overview*

The O/I/O/R (Online, Issues, Offline, Ready) column tells you the number of units with each of the 4 main statuses:

.. include:: unit-status.rst


