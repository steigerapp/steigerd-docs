.. Kumpel documentation master file, created by
   sphinx-quickstart on Thu Oct 25 19:47:37 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

**********************
Steiger Documentation
**********************

  **Stei·ger**

  | /ˈʃtaɪ̯ɡɐ/
  | *noun, male* 

  mine manager or mining foreman

.. TODO: for proper Lautschrift in LaTex docs, this needs to be done with a raw LaTeX directive. See http://docutils.sourceforge.net/docs/ref/rst/directives.html#raw-data-pass-through for details

`Steiger <https://www.steiger.app>`_ is a Software-as-a-Service management suite for cryptocurrency mining facilities of all sizes. It is able to manage all aspects of a mining operation - be it 3 or 30000 machines.
Every step in the lifecycle of a miner (we call them "units") - from the initial recording, the configuration and pool settings of the machine, 
to the monitoring of performance and temperature as well as location of individual machines within the farm - `Steiger <https://www.steiger.app>`_ has a solution for it.

`Steiger <https://www.steiger.app>`_ also offers pro features like labourer tracking, invoicing and a ticketing system that big scale operations and colocation providers missed so far.

`Steiger <https://www.steiger.app>`_ is web based - you only need a browser to use it. However, to properly control your units and gather their stats, you need to install a local management service, we call ':ref:`Kumpel<intro kumpel>`'

`Steiger <https://www.steiger.app>`_ main goal is simplicity and ease-of-use. Once Kumpel is set up, all important aspects of your miners can be administered within the Steiger software. There is no need to install software on your ASICs or ever login to any of your units again!

.. figure:: _images/ecosystem.png
    :width: 600px
    :align: center
    :alt: Steiger ecosystem overview

    *Steiger ecosystem overview*


Table Of Contents
####################


.. toctree::
   :maxdepth: 2
   :caption: Getting Started

   First Steps <user_guide/getting-started>


.. toctree::
    :glob:
    :caption: Using Steiger

    Steiger Web GUI <user_guide/intro-gui>
    Kumpel <user_guide/intro-kumpel> 
    Agency view <user_guide/intro-agency> 
    Eisen <user_guide/intro-eisen>
    Mobile app <user_guide/intro-mobileapp>

.. toctree::
    :glob:
    :caption: Common Scenarios

    Integrate Steiger in existing farms <user_guide/existing-farms>
    Setup a new farm <user_guide/new-farms>


.. toctree::
    :maxdepth: 2
    :caption: References & Appendixes

    Troubleshooting <user_guide/troubleshooting>
    FAQ <user_guide/faq>
    Glossary <user_guide/glossary>
    Manuals <user_guide/manuals>



   